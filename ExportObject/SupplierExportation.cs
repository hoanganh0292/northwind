﻿using System.Collections.Generic;

namespace NorthWind.ExportObject
{
    public class SupplierExportation
    {
        public string Title { get; set; }
        public IEnumerable<Supplier> Suppliers { get; set; }
    }
}
