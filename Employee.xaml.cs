﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NorthWind
{
    /// <summary>
    /// Interaction logic for Employee.xaml
    /// </summary>
    public partial class frmEmployee : Window
    {
        NorthwindEntities context = new NorthwindEntities();
        CollectionViewSource employeeViewSource;
        
        public frmEmployee()
        {
            InitializeComponent();
            employeeViewSource = ((CollectionViewSource)(FindResource("employeeViewSource")));
            DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource employeeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("employeeViewSource")));
            context.Employees.Load();
            employeeViewSource.Source = context.Employees.Local;
        }

        private void AddCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void EditCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void DeleteCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void RefreshCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void ExportCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {

        }
    }
}
