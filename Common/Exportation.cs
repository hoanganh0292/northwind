﻿using ClosedXML.Report;
using System;
using System.IO;

namespace NorthWind.Common
{
    public static class Exportation
    {
        /// <summary>
        /// Export any object using template
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fileName"></param>
        /// <returns>Export result file name</returns>
        public static string ExportToExcel<T>(T model, string fileName = "")
        {
            const string excelExtension = ".xlsx";
            const string exportTemplate = "ExportTemplates";
            const string exportFolder = "ExportResult";

            var startupPath = System.AppDomain.CurrentDomain.BaseDirectory;
            var objectTypeName = typeof(T).Name;

            var exportTemplateFile = Path.Combine(startupPath, exportTemplate, objectTypeName + excelExtension);
            if (!File.Exists(exportTemplateFile))
            {
                throw new FileNotFoundException("Export template file not found: " + objectTypeName);
            }

            var exportFolderPath = Path.Combine(startupPath, exportFolder);
            if (!Directory.Exists(exportFolderPath))
                Directory.CreateDirectory(exportFolderPath);
            var suffix = DateTime.Now.ToString("_ddMMyy_hhmmssff");
            fileName = string.IsNullOrWhiteSpace(fileName) ? objectTypeName : fileName;
            var exportFileName = Path.Combine(exportFolderPath, fileName + suffix + excelExtension);

            using (var template = new XLTemplate(exportTemplateFile))
            {
                template.AddVariable(model);
                template.Generate();
                template.Workbook.Worksheet(1).Rows().AdjustToContents();
                template.SaveAs(exportFileName);
            }

            return exportFileName;
        }
    }
}
