﻿using NorthWind.Common;
using NorthWind.Constant;
using NorthWind.ExportObject;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using static NorthWind.Constant.Enumerator;

namespace NorthWind.Views.Suppliers
{
    public partial class ucSupplier : UserControl
    {
        NorthwindEntities context = new NorthwindEntities();
        CollectionViewSource supplierViewSource;
        public ucSupplier()
        {
            InitializeComponent();
            supplierViewSource = ((CollectionViewSource)(FindResource("supplierViewSource")));
            DataContext = this;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Data.CollectionViewSource supplierViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("supplierViewSource")));
            context.Suppliers.Load();
            supplierViewSource.Source = context.Suppliers.Local;
        }

        private void AddCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            var frm = new frmSupplierDetail() {
                FormMode = EnumFormMode.Add,
                Context = context
            };
            var resultDialog = frm.ShowDialog();
            if (resultDialog != null && resultDialog == true)
            {
                ReloadData();
            }
        }

        private void EditCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var supplier = grdSupplier.SelectedItem as Supplier;
                if (supplier == null)
                {
                    MessageBox.Show(CommonMessage.YouMustChooseOneRow);
                    return;
                }
                var frm = new frmSupplierDetail()
                {
                    FormMode = EnumFormMode.Edit,
                    Context = context,
                    SupplierObject = supplier,
                };

                var resultDialog = frm.ShowDialog();
                if (resultDialog != null && resultDialog == true)
                {
                    ReloadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void DeleteCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var selectedSuppliers = grdSupplier.SelectedItems.Cast<Supplier>();
                if (selectedSuppliers == null || selectedSuppliers.Count() == 0)
                {
                    MessageBox.Show(CommonMessage.YouMustChooseAtLeastOneRow);
                    return;
                }
                if (MessageBox.Show(CommonMessage.ConfirmDelete, "NorthWind",MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    context.Suppliers.RemoveRange(selectedSuppliers);
                    var result = context.SaveChanges();
                    if(result <= 0)                
                    {
                        MessageBox.Show(CommonMessage.DeleteNotSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            ReloadData();
        }

        private void ReloadData()
        {
            btnRefresh.IsEnabled = false;
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            try
            {
                var objectContext = ((IObjectContextAdapter)context).ObjectContext;
                var refreshableObjects = context.ChangeTracker.Entries()//.Where(e => e.)
                                                .Select(c => c.Entity)                                                
                                                .ToList();
                objectContext.Refresh(RefreshMode.StoreWins, refreshableObjects);
                supplierViewSource.View.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                btnRefresh.IsEnabled = true;
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void ExportCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            btnExport.IsEnabled = false;
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            try
            {
                var supplierExportObject = new SupplierExportation
                {
                    Suppliers = supplierViewSource.View.Cast<Supplier>(),
                    Title ="Title Test"
                };
                string exportFilePath = Exportation.ExportToExcel<SupplierExportation>(supplierExportObject, "Supplier");
                if(File.Exists(exportFilePath))
                {
                    Process.Start(exportFilePath);
                    MessageBox.Show("Export success!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                btnExport.IsEnabled = true;
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
            }
        }
    }
}
