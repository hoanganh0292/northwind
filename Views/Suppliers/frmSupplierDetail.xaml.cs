﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using static NorthWind.Constant.Enumerator;

namespace NorthWind.Views.Suppliers
{
    /// <summary>
    /// Interaction logic for frmSupplierDetail.xaml
    /// </summary>
    public partial class frmSupplierDetail : Window
    {
        public EnumFormMode FormMode { get; set; }
        public NorthwindEntities Context { get; set; }
        private Supplier _supplier;
        public Supplier SupplierObject {
            get => _supplier;
            set
            {
                _supplier = value;
                OnPropertyChanged("SupplierObject");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;        
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public frmSupplierDetail()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (FormMode == EnumFormMode.Add)
            {
                _supplier = new Supplier();
            }
            grdSupplierDetail.DataContext = _supplier;
        }
       
        private void SaveCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {                
                if (FormMode == EnumFormMode.Add)
                {
                    Context.Suppliers.Local.Insert(0, _supplier);                    
                }
                Context.SaveChanges();
                DialogResult = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);                
            }            
        }

        private void CancelCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
    }
}
