﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthWind.Constant
{    
    public static class CommonMessage
    {
        public const string YouMustChooseOneRow = "You must choose one row to edit";
        public const string YouMustChooseAtLeastOneRow = "You must choose at least one row to delete";
        public const string DeleteNotSuccess = "Delete not success, please try again.";
        public const string ConfirmDelete = "Are you sure want to delete row(s)?";
    }
}
