﻿#pragma checksum "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "B03B8081ADA04290C91FC415E815854F624507E1B4E2C5C0DA91D24A4C406E8C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using NorthWind;
using NorthWind.Views.Suppliers;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace NorthWind.Views.Suppliers {
    
    
    /// <summary>
    /// frmSupplierDetail
    /// </summary>
    public partial class frmSupplierDetail : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdSupplierDetail;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox addressTextBox;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox cityTextBox;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox companyNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox contactNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox contactTitleTextBox;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox countryTextBox;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox faxTextBox;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox homePageTextBox;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox phoneTextBox;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox postalCodeTextBox;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox regionTextBox;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox supplierIDTextBox;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/NorthWind;component/views/suppliers/frmsupplierdetail.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
            ((NorthWind.Views.Suppliers.frmSupplierDetail)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 15 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.SaveCommandHandler);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 16 "..\..\..\..\Views\Suppliers\frmSupplierDetail.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.CancelCommandHandler);
            
            #line default
            #line hidden
            return;
            case 4:
            this.grdSupplierDetail = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.addressTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.cityTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.companyNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.contactNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.contactTitleTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.countryTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.faxTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.homePageTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.phoneTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.postalCodeTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.regionTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.supplierIDTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

